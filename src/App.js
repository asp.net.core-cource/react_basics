import React from "react";
import { Route, Routes, BrowserRouter } from "react-router-dom";
import { ErrorBoundary } from "react-error-boundary";
import "./App.css";
import LoginAccount from "./pages/LoginAccount";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { green, purple } from "@mui/material/colors";

function App() {
	const theme = createTheme({
		palette: {
			primary: {
				main: purple[500],
			},
			secondary: {
				main: green[500],
			},
		},
	});
	return (
		<React.StrictMode>
			<ErrorBoundary fallback={<div>Something went wrong</div>}>
				<ThemeProvider theme={theme}>
					<BrowserRouter>
						<Routes>
							<Route path="/" element={<LoginAccount />} />
						</Routes>
					</BrowserRouter>
				</ThemeProvider>
			</ErrorBoundary>
		</React.StrictMode>
	);
}

export default App;
